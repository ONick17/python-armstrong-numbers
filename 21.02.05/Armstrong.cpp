﻿#include <iostream>
using namespace std;

int main()
{
    int a1, a2, a3, num, check;
    for (a1 = 1; a1 < 32001; a1++) {
        a2 = a1;
        a3 = a1;
        num = 0;
        check = 0;
        while (a2 > 0) {
            num++;
            a2 /= 10;
        }
        while (a3 > 0) {
            check += pow((a3 % 10), num);
            a3 /= 10;
        }
        if (check == a1) {
            cout << a1 << endl;
        }
    }
    return(0);
}