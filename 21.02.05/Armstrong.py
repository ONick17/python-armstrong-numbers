for a1 in range(1, 32001):
    a2 = a1
    a3 = a1
    num = 0
    check = 0
    while (a2>0):
        num += 1
        a2 //= 10
    while (a3>0):
        check += (a3%10)**num
        a3 //= 10
    if (check == a1):
        print(a1)
