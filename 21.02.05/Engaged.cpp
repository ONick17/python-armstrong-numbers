﻿#include <iostream>
using namespace std;

int main()
{
    int a, n1, n2, n3;
    for (n1 = 48; n1 < 5776; n1++) {
        n2 = 0;
        n3 = 0;
        for (a = 1; a < (n1 / 2) + 1; a++) {
            if (n1 % a == 0) {
                n2 += a;
            }
        }
        n2--;
        if ((n2 > n1) && (n2 < 6129)) {
            for (a = 1; a < (n2 / 2) + 1; a++) {
                if (n2 % a == 0) {
                    n3 += a;
                }
            }
            n3--;
            if (n3 == n1) {
                cout << n1 << " " << n2 << endl;
            }
        }
    }
    return(0);
}
