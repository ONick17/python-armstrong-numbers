from random import randint as random

def randfill(lstM):
    for i in range(0, 5):
        for j in range(0, 5):
            lstM[i].append(random(30, 60))
    return(lstM)

def fndMx (lstM):
    a = lstM[0][0]
    for i in range(0, 5):
        for j in range(0, 5):
            if (lstM[i][j] > a):
                a = lstM[i][j]
    return(a)

def fndMn (lstM):
    a = lstM[0][0]
    for i in range(0, 5):
        for j in range(0, 5):
            if (lstM[i][j] < a):
                a = lstM[i][j]
    return(a)

lstM = []
for i in range(0, 5):
    lstM.append([])
lstM = randfill(lstM)
for i in range(0, 5):
    print(lstM[i])
print(fndMx(lstM))
print(fndMn(lstM))
