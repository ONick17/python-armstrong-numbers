﻿#include <iostream>
#include <list>
#include <string>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int i;
	list<int> lstM = { 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23 };

	list<int>::iterator itr_lstM = lstM.begin();
	for (i = 0; i < 3; i++) {
		itr_lstM++;
	}
	for (i = 3; i < 6; i++) {
		cout << *itr_lstM << ' ';
		itr_lstM++;
	}
	cout << endl;

	itr_lstM = lstM.begin();
	for (i = 0; i < 6; i++) {
		cout << *itr_lstM << ' ';
		itr_lstM++;
	}
	cout << endl;

	itr_lstM = lstM.begin();
	for (i = 0; i < 3; i++) {
		itr_lstM++;
	}
	for (i = 3; i < (lstM.size()); i++) {
		cout << *itr_lstM << ' ';
		itr_lstM++;
	}
	cout << endl;

	itr_lstM = lstM.end();
	for (i = lstM.size(); i > 0; i--) {
		itr_lstM--;
		cout << *itr_lstM << ' ';
	}
	cout << endl;

	itr_lstM = lstM.end();
	for (i = lstM.size(); i > lstM.size() - 3; i--) {
		itr_lstM--;
	}
	for (i = lstM.size() - 3; i < lstM.size(); i++) {
		cout << *itr_lstM << ' ';
		itr_lstM++;
	}
	cout << endl;

	itr_lstM = lstM.begin();
	for (i = 0; i < lstM.size() - 6; i++) {
		cout << *itr_lstM << ' ';
		itr_lstM++;
	}
	cout << endl;

	itr_lstM = lstM.end();
	itr_lstM--;
	cout << *itr_lstM << ' ';
	for (i = lstM.size() - 1; i > lstM.size() - 9; i -= 2) {
		itr_lstM--;
		itr_lstM--;
		cout << *itr_lstM << ' ';
	}
	cout << endl;
	cout << endl;

	itr_lstM = lstM.end();
	cout << "А) ";
	for (i = lstM.size() - 1; i > lstM.size() - 2; i--) {
		itr_lstM--;
	}
	for (i = lstM.size() - 2; i > lstM.size() - 1 - 9; i -= 1) {
		itr_lstM--;
		cout << *itr_lstM << ' ';
	}
	cout << endl;

	itr_lstM = lstM.begin();
	cout << "Б) ";
	for (i = 0; i < 3; i++) {
		itr_lstM++;
	}
	for (i = 3; i < 9; i += 3) {
		cout << *itr_lstM << ' ';
		itr_lstM++;
		itr_lstM++;
		itr_lstM++;
	}
	cout << *itr_lstM << endl;

	itr_lstM = lstM.end();
	cout << "В) ";
	for (i = lstM.size(); i > 0; i -= 1) {
		itr_lstM--;
		cout << *itr_lstM << ' ';
	}
	cout << endl;

	return (0);
}