print("Введите количество спортсменов и количество бросков (через пробел):")
n, m = map(int, input().split(' '))
A = [[0] * m] * n
B = [0] * n
print("Введите результаты бросков (через пробелы; каждая строка - спортсмен):")
for i in range(0, n):
    A[i] = list(map(int, input().split(' ')))
    for j in range(0, m):
        B[i] += A[i][j]
A_Ans = max(B)
B_Ans = B.index(max(B))
print("Лучший результат:", A_Ans)
print("Номер строки:", B_Ans + 1)
