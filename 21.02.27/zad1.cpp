﻿#include <iostream>
#include <ctime>
#include <random>
#include <list>
#include <string>
#include <fstream>
using namespace std;

const string ms_Names[7] = { "Олег", "Антон", "Женя", "Вова", "Слава", "Егор", "Игнат" };
const int Names_Num = 7;
const string ms_Surnames[7] = { "Мусорин", "Ударцев", "Бурков", "Загаинов", "Клименко", "Клоунов", "Сугаков" };
const int Surnames_Num = 7;
const string ms_Names2[7] = { "Денисович", "Максимович", "Владимирович", "Викторович", "Степанович", "Михайлович", "Васильевич" };
const int Names2_Num = 7;
const int minAge = 20;
const int maxAge = 60;

list<list<string>> fnc_CrtLst() {
    srand(time(0));
    list <list<string>> lst_A0;
    list <string> lst_A1;
    for (int i = 0; i < 40; i++) {
        lst_A1 = { ms_Surnames[rand() % Surnames_Num], ms_Names[rand() % Names_Num], ms_Names2[rand() % Names2_Num], to_string(minAge + rand() % (maxAge - minAge + 1)) };
        lst_A0.push_back(lst_A1);
    }
    cout << "СПИСОК СГЕНЕРИРОВАН" << endl;
    return(lst_A0);
}

string fnc_FlNmChkOut() {
    bool CHECK1 = true;
    bool CHECK4 = false;
    string fl_name;
    while (CHECK1) {
        cout << "ВВЕДИТЕ НАЗВАНИЕ ФАЙЛА ДЛЯ ВЫВОДА: ";
        cin >> fl_name;
        if (fl_name.length() < 5) {
            cout << "Ошибка: неверное имя файла для вывода" << endl;
            CHECK1 = false;
            break;
        }
        else {
            string NChk = fl_name.substr(fl_name.length() - 4, fl_name.length() - 1);
            if (NChk != ".txt") {
                cout << "Ошибка: неверное имя файла для вывода" << endl;
                CHECK1 = false;
                break;
            }
        }
        ofstream fl(fl_name, ios_base::app);
        if (!fl.is_open()) {
            cout << "Ошибка: не удалось открыть файл для вывода" << endl;
            CHECK1 = false;
            break;
        }
        fl.close();
        ifstream fl2(fl_name);
        string CHECK2;
        fl2 >> CHECK2;
        fl2.close();
        if (CHECK2 == "") {
            string CHECK3;
            cout << "Ошибка: файл для вывода не пустой" << endl << "Что делать дальше?" << endl;
            cout << "1 - Ввести новое имя файла" << endl << "2 - Перезаписать файл" << endl;
            cout << "3 - Закрыть программу" << endl;
            while ((CHECK3 != "1") && (CHECK3 != "2") && (CHECK3 != "3")) {
                cin >> CHECK3;
                if (CHECK3 == "3") {
                    CHECK1 = false;
                    break;
                }
                else {
                    if (CHECK3 == "2") {
                        fl2.close();
                        CHECK1 = false;
                        CHECK4 = true;
                    }
                    else {
                        if ((CHECK3 != "1") && (CHECK3 != "2") && (CHECK3 != "3")) {
                            cout << "Ошибка: неверный ответ на запрос" << endl;
                            cout << "Напишите '1', '2' или '3'" << endl;
                        }
                    }
                }
            }
        }
        else {
            CHECK1 = false;
            CHECK4 = true;
        }
    }
    if (!CHECK4) {
        fl_name = "ERROR";
    }
    return(fl_name);
}

string fnc_FlNmChkIn() {
    bool CHECK1 = true;
    bool CHECK4 = false;
    string fl_name;
    while (CHECK1) {
        cout << "ВВЕДИТЕ НАЗВАНИЕ ФАЙЛА ДЛЯ ВВОДА: ";
        cin >> fl_name;
        if (fl_name.length() < 5) {
            cout << "Ошибка: неверное имя файла для ввода" << endl;
            CHECK1 = false;
            break;
        }
        else {
            string NChk = fl_name.substr(fl_name.length() - 4, fl_name.length() - 1);
            if (NChk != ".txt") {
                cout << "Ошибка: неверное имя файла для ввода" << endl;
                CHECK1 = false;
                break;
            }
        }
        ifstream fl2(fl_name);
        if (!fl2.is_open()) {
            cout << "Ошибка: не удалось открыть файл для ввода" << endl;
            CHECK1 = false;
            break;
        }
        string CHECK2;
        fl2 >> CHECK2;
        fl2.close();
        if (CHECK2 == "") {
            string CHECK3;
            cout << "Ошибка: файл для ввода пустой" << endl << "Что делать дальше?" << endl;
            cout << "1 - Ввести новое имя файла" << endl;
            cout << "2 - Закрыть программу" << endl;
            while ((CHECK3 != "1") && (CHECK3 != "2")) {
                cin >> CHECK3;
                if (CHECK3 == "2") {
                    CHECK1 = false;
                    break;
                }
                else if ((CHECK3 != "1") && (CHECK3 != "2")) {
                    cout << "Ошибка: неверный ответ на запрос" << endl;
                    cout << "Напишите '1' или '2'" << endl;
                }
            }
        }
        else {
            CHECK1 = false;
            CHECK4 = true;
        }
    }
    if (!CHECK4) {
        fl_name = "ERROR";
    }
    return(fl_name);
}

void fnc_SaveFl(list<list<string>> lst_Ans) {
    string fl_name;
    fl_name = fnc_FlNmChkOut();
    if (fl_name != "ERROR") {
        ofstream fl3(fl_name);
        list <list<string>> ::iterator lst_Ans_iter1 = lst_Ans.begin();
        list <string> ::iterator lst_wtf_iter1;
        for (int i = 0; i < lst_Ans.size(); i++) {
            lst_wtf_iter1 = (*lst_Ans_iter1).begin();
            for (int j = 0; j < (*lst_Ans_iter1).size(); j++) {
                fl3 << *lst_wtf_iter1 << ' ';
                lst_wtf_iter1++;
            }
            fl3 << endl;
            lst_Ans_iter1++;
        }
    }
}

list<list<string>> fnc_ReadFl() {
    list<list<string>> lst_A0;
    list<string> lst_A1;
    string fl_name, fl_line;
    fl_name = fnc_FlNmChkIn();
    if (fl_name != "ERROR") {
        ifstream fl(fl_name);
        while (getline(fl, fl_line)) {
            lst_A1.push_back(fl_line.substr(0, fl_line.find(' ')));
            fl_line.erase(0, fl_line.find(' ')+1);
            lst_A1.push_back(fl_line.substr(0, fl_line.find(' ')));
            fl_line.erase(0, fl_line.find(' ')+1);
            lst_A1.push_back(fl_line.substr(0, fl_line.find(' ')));
            fl_line.erase(0, fl_line.find(' ')+1);
            lst_A1.push_back(fl_line);
            lst_A0.push_back(lst_A1);
            lst_A1.clear();
        }
    }
    return(lst_A0);
}

void fnc_LstOut(list<list<string>> lst_Ans) {
    list <list<string>> ::iterator lst_Ans_iter1;
    list <string> ::iterator lst_wtf_iter1;
    for (lst_Ans_iter1 = lst_Ans.begin(); lst_Ans_iter1 != lst_Ans.end(); lst_Ans_iter1++) {
        for (lst_wtf_iter1 = lst_Ans_iter1->begin(); lst_wtf_iter1 != lst_Ans_iter1->end(); lst_wtf_iter1++) {
            cout << *lst_wtf_iter1 << ' ';
        }
        cout << endl;
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");
    string chs = "keke";
    list <list<string>> lst;
    bool chk, chk2 = true;

    while (true) {
        chk = true;
        if (chk2) {
            cout << "---ГЕНЕРАТОР СПИСКА ИМЁН---" << endl;
            cout << "Список комманд:" << endl;
            cout << "create - сгенерировать новый список" << endl;
            cout << "print - вывести список на экран" << endl;
            cout << "to_file - записать список в файл" << endl;
            cout << "from_file - взять готовый список из файла" << endl;
            cout << "stop - закрыть программу" << endl;
            cout << "help - вывести окно с описанием" << endl;
            chk2 = false;
        }
        cout << "Выполнить: ";
        cin >> chs;
        if (chs == "create") {
            lst = fnc_CrtLst();
            chk = false;
        }
        if (chs == "print") {
            fnc_LstOut(lst);
            chk = false;
        }
        if (chs == "to_file") {
            fnc_SaveFl(lst);
            chk = false;
        }
        if (chs == "from_file") {
            lst = fnc_ReadFl();
            chk = false;
        }
        if (chs == "stop") {
            return 0;
        }
        if (chs == "help") {
            chk2 = true;
            chk = false;
        }
        if (chk) {
            cout << "Комманда была введена неверно! Попробуйте ещё раз!" << endl;
        }
    }
}
