﻿#include <iostream>
#include <map>
#include <fstream>
#include <string>
using namespace std;

int main()
{
	multimap <string, string> roles;
	ifstream fl1;
	fl1.open("roles.txt");
	int cnt = 0;
	string str1;
	while (getline(fl1, str1)) {
		cnt++;
		int str1_fnd = str1.find(':');
		if (str1_fnd != -1) {
			string str_key = str1.substr(0, str1_fnd + 1);
			roles.insert(make_pair(str_key, to_string(cnt) + ") " + str1.substr(str1_fnd + 2)));
		}
	}
	fl1.close();

	ofstream fl2;
	fl2.open("roles-res.txt");
	multimap<string, string>::iterator roles_itr2 = roles.begin();
	fl2 << roles_itr2->first << endl;
	fl2 << roles_itr2->second << endl;
	roles_itr2++;
	multimap<string, string>::iterator roles_itr1 = roles.begin();
	for (roles_itr2; roles_itr2 != roles.end(); roles_itr2++)
	{
		if (roles_itr1->first == roles_itr2->first) {
			fl2 << roles_itr2->second << endl;
		}
		else {
			fl2 << endl;
			fl2 << roles_itr2->first << endl;
			fl2 << roles_itr2->second << endl;
		}
		roles_itr1++;
	}

	return(0);
}