roles = {}
str1 = "ы"
cnt = 0

with open("text1.txt", encoding = "utf-8") as fl1:
    while (str1 != ""):
        str1 = fl1.readline()
        str1_fnd = str1.find(':')
        cnt += 1
        if (str1_fnd != -1):
            key = str1[0 : str1_fnd+1]
            if (roles.get(key, "None") == "None"):
                if (str1[-1] == '\n'):
                    value = [str(cnt) + ") " + str1[str1_fnd+2 : -1]]
                else:  value = [str(cnt) + ") " + str1[str1_fnd+2 : ]]
                roles[key] = value
            else:
                if (str1[-1] == '\n'):
                    value = str(cnt) + ") " + str1[str1_fnd+2 : -1]
                else:  value = str(cnt) + ") " + str1[str1_fnd+2 : ]
                roles[key].append(value)
fl1.close()

with open("text1-res.txt", 'w') as fl2:
    for key in roles:
        fl2.write(key + '\n')
        for i in roles[key]:
            fl2.writelines(i + '\n')
        fl2.writelines('\n')
fl2.close()
