import json
import requests

def fnc_crt_user_json():
    fl = open("weather-user.json", 'w+')
    user_data = {"user": { "login": "nagib117", "last_date": "17.07.2017", "last_time": 1717}}
    json.dump(user_data, fl)
    fl.close()
    fnc_anls_json(user_data)
    
def fnc_anls_json(obj_json):
    if ("user" in obj_json):
        print("Логин:", obj_json["user"]["login"])
        print("Последняя дата захода:", obj_json["user"]["last_date"])
        print("Продолжительность последнего сеанса: ", obj_json["user"]["last_time"], "с", sep="")
        print()
    else:
        print("Температура: ", int(obj_json["list"][0]["main"]["temp"] - 273), "°C", sep="")
        print("Ощущается как: ", int(obj_json["list"][0]["main"]["feels_like"] - 273), "°C", sep="")
        print("Скорость ветра: ", obj_json["list"][0]["wind"]["speed"], "м/с", sep="")
        print("Направление ветра: ", obj_json["list"][0]["wind"]["deg"], "°", sep="")
        print("Влажность: ", obj_json["list"][0]["main"]["humidity"], "%", sep="")
        print()

fnc_crt_user_json();
city = "Irkutsk,RU"
appID = "a163549e240d189fa9ff96eacc346978"
req = requests.get("http://api.openweathermap.org/data/2.5/find?q=" + city + "&type=like&APPID=" + appID)
obj_weather_json = req.json();
fl = open("weather.json", 'w+')
json.dump(obj_weather_json, fl, indent = True)
fl.close()
fnc_anls_json(obj_weather_json)
