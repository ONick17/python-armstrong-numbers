﻿#include <iostream>
#include <array>
using namespace std;

class cl_vehicle {
public:
    int _weight_;
    string _color_;
    cl_vehicle(int weight_in = 3000, string color_in = "black") {
        _weight_ = weight_in;
        _color_ = color_in;
    }
};

class cl_car : public cl_vehicle {
public:
    string _direction_;
    cl_car(string direction_in = "stop", int weight_in = 3000, string color_in = "black") {
        array<string, 5>directions = { "forward", "back", "left", "right", "stop" };
        _weight_ = weight_in;
        _color_ = color_in;
        bool chk = false;
        for (int i = 0; i < 5; i++) {
            if (directions[i] == direction_in) {
                chk = true;
            }
        }
        if (chk) {
            _direction_ = direction_in;
        }
        else {
            _direction_ = "stop";
            cout << "Неверное направление! Машина стоит.";
        }
    }
};

class cl_tram : public cl_vehicle {
public:
    string _direction_;
    string _power_;
    cl_tram(string direction_in = "stop", string power_in = "off", int weight_in = 3000, string color_in = "black") {
        array<string, 5>directions = { "forward", "back", "left", "right", "stop" };
        array<string, 2>powers = { "on", "off" };
        _weight_ = weight_in;
        _color_ = color_in;
        bool chk = false;
        for (int i = 0; i < 5; i++) {
            if (directions[i] == direction_in) {
                chk = true;
            }
        }
        if (chk) {
            _direction_ = direction_in;
        }
        else {
            _direction_ = "stop";
            cout << "Неверное направление! Трамвай стоит.";
        }
        chk = false;
        for (int i = 0; i < 2; i++) {
            if (powers[i] == power_in) {
                chk = true;
            }
        }
        if (chk) {
            _power_ = power_in;
        }
        else {
            _power_ = "off";
            cout << "Неверное описание токоприёмника! Ток отключен.";
        }
    }
};

int main()
{
    setlocale(LC_ALL, "Russian");
    cl_car a("forward", 7000);
    cout << a._color_ << endl;
    cout << a._weight_;
}