class cl_vehicle:
    def __init__(self, weight = 3000, color = "black"):
        self.weight = weight
        self.color = color

class cl_car(cl_vehicle):
    def __init__(self, direction = "stop", weight = 3000, color = "black"):
        super().__init__(weight, color)
        directions = ["forward", "back", "left", "right", "stop"]
        if direction in directions:
            self.direction = direction
        else:
            self.direction = "stop"
            print("Неверное направление! Машина стоит.")

class cl_tram(cl_vehicle):
    def __init__(self, direction = "stop", power = "off", weight = 7000, color = "black"):
        directions = ["forward", "back", "stop"]
        powers = [on, off]
        super().__init__(weight, color)
        if direction in directions:
            self.direction = direction
        else:
            self.direction = "stop"
            print("Неверное направление! Трамвай стоит.")
        if power in powers:
            self.power = power
        else:
            self.power = "off"
            print("Неверное описание токоприёмника! Ток отключен.")

a = cl_car("forward", 7000)
a.color = "red"
print (a.color)
print (a.weight)
