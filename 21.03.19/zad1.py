from random import randint as random
from operator import attrgetter
from math import sqrt as sqrt


class cl_coord:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y



N = 2000
coords = []
for i in range (N):
    a = cl_coord(random(0, 2**31), random(0, 2**31))
    coords.append(a)
    
coords = sorted(coords, key=attrgetter("x", "y"))
#for i in range (N):
#    print(i+1, ": ", coords[i].x, " ", coords[i].y, sep="")

mn = sqrt((coords[0].x - coords[1].x)**2 + (coords[0].y - coords[1].y)**2)
ans = 0
for i in range (1, N - 1):
    a = sqrt((coords[i].x - coords[i+1].x)**2 + (coords[i].y - coords[i+1].y)**2)
    if (a < mn):
        mn = a
        ans = i
print(mn)
print(ans+1, " ", ans+2)
