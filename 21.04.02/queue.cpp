﻿#include <iostream>
#include <queue>
#include <map>
#include <string>
using namespace std;

void help() {
    cout << "Список комманд:" << endl;
    cout << " - insert x – добавляет в очередь элемент с приоритетом x;" << endl;
    cout << " - getMax – возвращает значение максимального приоритета;" << endl;
    cout << " - remove x - удаляет из очереди элемент x;" << endl;
    cout << " - print - печатает текущее состояние очереди;" << endl;
    cout << " - help - печатает набор комманд;" << endl;
    cout << " - exit - завершает программу." << endl;
}

void getMax(map <int, queue<int>> dct) {
    if (!dct.empty()) {
        map<int, queue<int>>::iterator it1 = dct.begin();
        int iMax = it1->first;
        for (it1; it1 != dct.end(); it1++) {
            if (it1->first > iMax) {
                iMax = it1->first;
            }
        }
        cout << dct[iMax].front() << endl;
    }
    else {
        cout << "Очередь пуста!" << endl;
    }
}

void printQueue(map <int, queue<int>> dct) {
    map<int, queue<int>>::iterator it1 = dct.begin();
    queue<int> stck1;
    for (int i = 0; i != dct.size(); i++) {
        cout << it1->first << ":";
        stck1 = it1->second;
        int sz = stck1.size();
        for (int i2 = 0; i2 < sz; i2++) {
            cout << " " << stck1.front();
            stck1.pop();
        }
        cout << endl;
        it1++;
    }
}

map <int, queue<int>> insert(map <int, queue<int>> dct, int x, int cnt) {
    int chk = 0;
    map<int, queue<int>>::iterator it1 = dct.begin();
    for (it1; it1 != dct.end(); it1++) {
        if (it1->first == x) {
            it1->second.push(cnt);
            chk = 1;
            break;
        }
    }
    if (chk == 0) {
        queue<int> stck1;
        stck1.push(cnt);
        dct.insert(make_pair(x, stck1));
    }
    return(dct);
}

map <int, queue<int>> remove(map <int, queue<int>> dct, int x) {
    int chk = 0;
    map<int, queue<int>>::iterator it1 = dct.begin();
    for (it1; it1 != dct.end(); it1++) {
        if (it1->first == x) {
            it1->second.pop();
            chk = 1;
            break;
        }
    }
    if (chk == 1) {
        if (dct[x].empty()) {
            dct.erase(x);
        }
    }
    if (chk == 0) {
        cout << "Очередь на данный приоритет пуста!" << endl;
    }
    return(dct);
}

int main()
{
    setlocale(LC_ALL, "Russian");
    help();
    map <int, queue<int>> dct;
    int cnt = 1;
    while (true) {
        string inpt;
        getline(cin, inpt);
        if (inpt == "exit") {
            break;
        }
        else if (inpt == "help") {
            help();
        }
        else if (inpt == "getMax") {
            getMax(dct);
        }
        else if (inpt == "print") {
            printQueue(dct);
        }
        else if (inpt.find("insert") != -1) {
            inpt.erase(0, 6);
            int x = stoi(inpt);
            dct = insert(dct, x, cnt);
            cnt += 1;
        }
        else if (inpt.find("remove") != -1) {
            inpt.erase(0, 6);
            int x = stoi(inpt);
            dct = remove(dct, x);
        }
        else {
            cout << "Неверная комманда!" << endl;
        }
    }
    return(0);
}