def help():
    print("Список комманд:")
    print(" - insert x – добавляет в очередь элемент с приоритетом x;")
    print(" - getMax – возвращает значение максимального приоритета;")
    print(" - remove x - удаляет из очереди элемент x;")
    print(" - printQueue - печатает текущее состояние очереди;")
    print(" - help - печатает набор комманд;")
    print(" - exit - завершает программу.")

def getMax(dct):
    chk = 0
    if (bool(dct.keys())):
        for i in dct.keys():
            if (bool(i)):
                chk = 1
        if (chk == 1):
            a = max(dct.keys())
            print(dct[a][-1])
        else:
            print("Очередь пуста!")
    else:
        print("Очередь пуста!")

def printQueue(dct):
    for i in dct.keys():
        print(i, ":", end="", sep="")
        for j in dct[i]:
            print(" ", j, end="", sep="")
        print()

def insert(dct, x, cnt):
    if (x in dct.keys()):
        dct[x].append(cnt)
    else:
        dct[x] = []
        dct[x].append(cnt)
    return(dct)

def remove(dct, x):
    if (x in dct.keys()):
        dct[x].pop()
        if (not bool(dct[x])):
            dct.pop(x)
    else:
        print("Очередь на данный приоритет пуста!")
    return(dct)

help()
dct = {}
cnt = 1
while (True):
    inpt = input()
    if (inpt == "exit"):
        break
    elif (inpt == "help"):
        help()
    elif (inpt == "getMax"):
        getMax(dct)
    elif (inpt == "printQueue"):
        printQueue(dct)
    elif ("insert " in inpt):
        x = int(inpt[7:])
        dct = insert(dct, x, cnt)
        cnt += 1
    elif ("remove " in inpt):
        x = int(inpt[7:])
        dct = remove(dct, x)
    else:
        print("Неверная комманда!")
