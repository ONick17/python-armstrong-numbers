﻿#include <iostream>
#include <stack>
#include <ctime>
using namespace std;

void stack_overflow(stack <int> stck1, int x) {
	x++;
	stck1.push(x);
	stack_overflow(stck1, x);
}

int stack_getMin(stack <int> stck1, int x) {
	if (x > stck1.top()) {
		x = stck1.top();
	}
	stck1.pop();
	if (!stck1.empty()) {
		x = stack_getMin(stck1, x);
	}
	return(x);
}

stack <int> stack_create(stack <int> stck1, int len) {
	if (len > 0) {
		stck1.push(rand());
		len -= 1;
		stck1 = stack_create(stck1, len);
	}
	return(stck1);
}

void stack_show(stack <int> stck1) {
	cout << stck1.top() << "   ";
	stck1.pop();
	if (!stck1.empty()) {
		stack_show(stck1);
	}
}

int main()
{
	stack <int> stck1;
	srand(time(0));
	stck1 = stack_create(stck1, 7);
	stack_show(stck1);
	int x = stck1.top();
	x = stack_getMin(stck1, x);
	cout << endl;
	cout << x;
	return(0);
}