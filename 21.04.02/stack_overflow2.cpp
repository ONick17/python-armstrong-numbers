﻿#include <iostream>
#include <stack>
#include <ctime>
using namespace std;

void stack_overflow(stack <int> stck1, int x) {
	x++;
	stck1.push(x);
	stack_overflow(stck1, x);
}

void stack_create_getMin (stack <int> *stck1, int *len, int *x, int *chk) {
	if (*len > 0) {
		(*stck1).push(rand());
		if (*chk == 0) {
			*x = stck1->top();
			(*chk)++;
		}
		else {
			if (*x > stck1->top()) {
				*x = stck1->top();
			}
		}
		(*len)--;
		stack_create_getMin(stck1, len, x, chk);
	}
}

void stack_show(stack <int> stck1) {
	cout << stck1.top() << "   ";
	stck1.pop();
	if (!stck1.empty()) {
		stack_show(stck1);
	}
}

int main()
{
	stack <int> stck1;
	int chk = 0;
	int x = 0;
	int len = 7;
	srand(time(0));
	stack_create_getMin(&stck1, &len, &x, &chk);
	stack_show(stck1);
	cout << endl;
	cout << x;
	return(0);
}