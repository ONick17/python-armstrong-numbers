from math import log

def tree_show(x):
    print()
    while (log(len(x)+1, 2)%1 != 0):
        x.append(None)
    i = 0
    stp = 1
    rw = 1
    spcs = (len(x)-1)//2
    while (i <= (len(x)-1)//2):
        if (i == (len(x)-1)//2):
            for j in range(i, len(x)):
                if (x[j] != None):
                    print(x[j], end='\t')
                else:
                    print(' ', end='\t')
                print(' ', end='\t')
        else:
            for j in range(i, i+rw):
                if (j == i):
                    for k in range(0, spcs):
                        print(' ', end='\t')
                    print(x[j], end='\t')
                else:
                    for k in range(0, (spcs*2)+1):
                        print(' ', end='\t')
                    print(x[j], end='\t')
        i += rw
        stp += 1
        rw = 2**(stp-1)
        spcs = (spcs-1)//2
        print()
    print()
    
def change(x, y):
     x += y
     y = x - y
     x -= y
     return (x, y)

def tree_sort(x):
    cnt = 0
    chk = True
    while (log(len(x)+1, 2)%1 != 0):
        x.append(None)
    while (chk):
        chk = False
        for i in range(1, len(x)):
            if (x[i] != None):
                if(x[i] > x[i-1]):
                    chk = True
        if not(chk):
            break
        for i in range(0, len(x)//2):
            if (x[i+1] != None):
                if (x[i] < x[i+1]):
                    print(x[i], '\t', "<->", '\t', x[i+1])
                    x[i], x[i+1] = change(x[i], x[i+1])
                    cnt += 1
            if (x[i+2] != None):
                if (x[i] < x[i+2]):
                    print(x[i], '\t', "<->", '\t', x[i+2])
                    x[i], x[i+2] = change(x[i], x[i+2])
                    cnt += 1
        i = 1
        stp = 2
        while (i <= len(x)//2):
            for j in range (i, i + stp - 1):
                if (x[j+1] != None):
                    if (x[j] < x[j+1]):
                        print(x[j], '\t', "<->", '\t', x[j+1])
                        x[j], x[j+1] = change(x[j], x[j+1])
                        cnt += 1
            i += stp
            stp *= 2
    while (x[len(x) - 1] == None):
        x.remove(None)
    return(x, cnt)

print("Напишите длинну списка элементов:")
lst_sz = int(input())
print("Напишите список элементов (через пробел):")
x = input().split(' ')
if (len(x) != lst_sz):
    print("Список элементов не совпадает с заявленной длиной!")
else:
    x2 = []
    for i in x:
        x2.append(int(i))
    x.clear()
    x2, cnt = tree_sort(x2)
    print(x2)
    tree_show(x2)
    if (cnt <= 4*lst_sz):
        print("Алгоритм справился")
    else:
        print("Алгоритм не справился")
