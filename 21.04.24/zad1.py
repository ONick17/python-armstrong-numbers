class cord:
    def __init__(self, x, y):
        self.x = x
        self.y = y

li_cords = []
print("Вводите координаты построчно (x и y через пробел).")
print("Чтобы остановить ввод, введите точку (-1; -1).")
while True:
    a1, a2 = input().split()
    a1 = int(a1)
    a2 = int(a2)
    if (a1 == -1) and (a2 == -1):
        break
    elif (a1 < 0) or (a2 < 0):
        print("Координаты не могут быть меньше нуля!")
    else:
        a = cord(a1, a2)
        chk = True
        for i in li_cords:
            if (i.x == a.x) and (i.y == a.y):
                print("Такая точка уже есть!")
                chk = False
        if chk:
            li_cords.append(a)
dic_cords = {}
cnt = 0
while (li_cords):
    a = li_cords.pop(0)
    cnt += 1
    dic_cords[cnt] = [a]
    li_news = []
    t = 0
    while (li_cords):
        i = li_cords[t]
        if (a.x == i.x) and (a.y == i.y):
            dic_cords[cnt].append(i)
            li_news.append(i)
            li_cords.remove(i)
        elif ((a.x + 1 == i.x) or (a.x - 1 == i.x)) and (a.y == i.y):
            dic_cords[cnt].append(i)
            li_news.append(i)
            li_cords.remove(i)
        elif ((a.y + 1 == i.y) or (a.y - 1 == i.y)) and (a.x == i.x):
            dic_cords[cnt].append(i)
            li_news.append(i)
            li_cords.remove(i)
        else:
            t += 1
        if (t == len(li_cords)):
            break
    while (li_news):
        j = li_news[0]
        li_news.remove(j)
        t = 0
        while (li_cords):
            i = li_cords[t]
            if (j.x == i.x) and (j.y == i.y):
                dic_cords[cnt].append(i)
                li_news.append(i)
                li_cords.remove(i)
            elif ((j.x + 1 == i.x) or (j.x - 1 == i.x)) and (j.y == i.y):
                dic_cords[cnt].append(i)
                li_news.append(i)
                li_cords.remove(i)
            elif ((j.y + 1 == i.y) or (j.y - 1 == i.y)) and (j.x == i.x):
                dic_cords[cnt].append(i)
                li_news.append(i)
                li_cords.remove(i)
            else:
                t += 1
            if (t == len(li_cords)):
                break
ans = len(dic_cords)
print("Понадобится цветов: ", ans)
