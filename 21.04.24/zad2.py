class cord:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

plan = []
chk = False
while True:
    a = input()
    if (a == ""):
        break
    else:
        for i in a:
            if (i != "l") and (i != "r") and (i != "u") and (i != "d"):
                print("НЕВЕРНАЯ КОМАНДА")
                chk = True
                break
            else:
                plan.append(i)
if chk:
    print("Программа завершилась с ошибкой ввода")
else:
    field = []
    for i in range(4):
        field.append([0, 0, 0, 0])
    cursN = cord(int(0), int(0))
    field[cursN.y][cursN.x] = 2
    routeSTR = []
    routeXY = []
    for i in plan:
        cursO = cursN
        if (i == "u"):
            x = cursN.x
            y = cursN.y - 1
            if (y == -1):
                y = 3
            cursN = cord(x, y)
        elif (i == "d"):
            x = cursN.x
            y = cursN.y + 1
            if (y == 4):
                y = 0
            cursN = cord(x, y)
        elif (i == "l"):
            x = cursN.x - 1
            y = cursN.y
            if (x == -1):
                x = 3
            cursN = cord(x, y)
        elif (i == "r"):
            x = cursN.x + 1
            y = cursN.y
            if (x == 4):
                x = 0
            cursN = cord(x, y)
        field[cursN.y][cursN.x] = 2
        field[cursO.y][cursO.x] = 1
        routeSTR.append(str(cursO.x)+" "+str(cursO.y)+" --> "+str(cursN.x)+" "+str(cursN.y))
        routeXY.append([cursO.x, cursO.y])
        print("______________")
        for i in field:
            print(i)
    i = 0
    chk = False
    while (i < len(routeSTR)):
        a = routeSTR[i]
        routeSTR[i] += "!"
        for j in routeSTR:
            if (a == j):
                chk = True
                ans = i
                break
        routeSTR[i] = a
        i += 1
    if chk:
        print(routeXY[ans][0], routeXY[ans][1])
        for i in routeSTR:
            print(i)
    else:
        print("Зацикливания нема")
        for i in field:
            print(i)
        for i in routeSTR:
            print(i)
