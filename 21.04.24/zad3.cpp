﻿#include <iostream>
#include <string>
using namespace std;

int main() {
    int num, base = 3;
    string num1 = "", num2 = "";
    cin >> num;
    while (num > 0) {
        num1 = to_string(num % base) + num1;
        num /= base;
    }
    for (int i = 0; i <= num1.length() - 1; i++) {
        num2 += num1.substr(i, 1) + num1.substr(i, 1);
    }
    for (int i = 0; i <= num2.length() - 1; i++) {
        num += stoi(num2.substr(i, 1)) * pow(3, num2.length() - i - 1);
    }
    cout << num;
    return(0);
}
