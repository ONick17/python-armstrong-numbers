num = int(input())
num2 = ""
while (num>0):
    num2 += str(num%3)
    num //= 3
num2 = num2[::-1]
num3 = ""
for i in num2:
    num3 += i*2
print(int(num3, 3))
