﻿#include <iostream>
#include <list>
#include <string>
using namespace std;

bool inp_check(list<int> inp) {
    int matr[10][10] = {
        {0, 3, 1, 7, 5, 9, 8, 6, 4, 2},
        {7, 0, 9, 2, 1, 5, 4, 8, 6, 3},
        {4, 2, 0, 6, 8, 7, 1, 3, 5, 9},
        {1, 7, 5, 0, 9, 8, 3, 4, 2, 6},
        {6, 1, 2, 3, 0, 4, 5, 9, 7, 8},
        {3, 6, 7, 4, 2, 0, 9, 5, 8, 1},
        {5, 8, 6, 9, 7, 2, 0, 1, 3, 4},
        {8, 9, 4, 5, 3, 6, 2, 0, 1, 7},
        {9, 4, 3, 8, 6, 1, 7, 2, 0, 5},
        {2, 5, 8, 1, 4, 3, 6, 7, 9, 0}
    };
    int prom = 0;
    list<int>::iterator itr;
    for (itr = inp.begin(); itr != inp.end(); itr++) {
        prom = matr[prom][*itr];
    }
    if (prom == 0) {
        return(true);
    }
    else {
        return(false);
    }
}

list<int> inp_repl(list<int> inp, int i) {
    list<int>::iterator itr = inp.begin();
    for (i; i > 0; i--) {
        itr++;
    }
    int keke1 = *itr;
    itr--;
    int keke2 = *itr;
    *itr = keke1;
    itr++;
    *itr = keke2;
    return(inp);
}

int main()
{
    while (true) {
        cout << "Введите кодировку(чтобы отменить ввод, введите 'stop'): ";
        string inp_str;
        cin >> inp_str;
        if (inp_str == "stop") {
            break;
        }
        else {
            list<char> inp_lst;
            for (char i : inp_str) {
                inp_lst.push_back(i);
            }
            list<int> inp;
            for (char i : inp_lst) {
                if ((i == '0') || (i == '1') || (i == '2') || (i == '3') || (i == '4') || (i == '5') || (i == '6') || (i == '7') || (i == '8') || (i == '9')) {
                    inp.push_back(atoi(&i));
                }
            }
            if (inp_check(inp)) {
                cout << "Последовательность верна!" << endl;
            }
            else {
                cout << "Последовательность нарушена!" << endl;
                int i = inp.size() - 2;
                while (true) {
                    if (i == 0) {
                        cout << "Исправить поседовательность невозможно!" << endl;
                        break;
                    }
                    else {
                        inp = inp_repl(inp, i);
                        if (inp_check(inp)) {
                            for (int i : inp) {
                                cout << i;
                            }
                            cout << endl;
                            break;
                        }
                        else {
                            inp = inp_repl(inp, i);
                            i--;
                        }
                    }
                }
            }
        }
    }
	return(0);
}