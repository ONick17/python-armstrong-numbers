def inp_check(inp):
    matr = []
    matr.append([0, 3, 1, 7, 5, 9, 8, 6, 4, 2])
    matr.append([7, 0, 9, 2, 1, 5, 4, 8, 6, 3])
    matr.append([4, 2, 0, 6, 8, 7, 1, 3, 5, 9])
    matr.append([1, 7, 5, 0, 9, 8, 3, 4, 2, 6])
    matr.append([6, 1, 2, 3, 0, 4, 5, 9, 7, 8])
    matr.append([3, 6, 7, 4, 2, 0, 9, 5, 8, 1])
    matr.append([5, 8, 6, 9, 7, 2, 0, 1, 3, 4])
    matr.append([8, 9, 4, 5, 3, 6, 2, 0, 1, 7])
    matr.append([9, 4, 3, 8, 6, 1, 7, 2, 0, 5])
    matr.append([2, 5, 8, 1, 4, 3, 6, 7, 9, 0])
    prom = 0
    for i in range(0, len(inp)):
        stolb = int(inp[i])
        prom = matr[prom][stolb]
    if (prom == 0):
        return(True)
    else:
        return(False)

def inp_repl(inp, i):
    keke = inp[i]
    inp[i] = inp[i-1]
    inp[i-1] = keke
    return(inp)

while True:
    inp = list(input("Введите кодировку (чтобы отменить ввод, введите 'stop'): "))
    if (inp == ["s", "t", "o", "p"]):
        break
    try:
        if (inp_check(inp)):
            print("Последовательность верна!")
        else:
            print("Последовательность нарушена!")
            i = len(inp)-2
            while True:
                if (i==0):
                    print("Исправить последовательность невозможно!")
                    break
                else:
                    inp = inp_repl(inp, i)
                    if (inp_check(inp)):
                        for i in inp:
                            print(i, sep="", end="")
                        print()
                        break
                    else:
                        inp = inp_repl(inp, i)
                        i -= 1
    except ValueError:
        print("Введено неверное значение!")
