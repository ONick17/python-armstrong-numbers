from random import randint as rand
from math import log

def create_asc(inp):
    print("Оригинал:")
    ltrs = []
    for ltr in inp:
        a = bin(ord(ltr))[2:]
        while (len(a) < 8):
            a = '0' + a
        ltrs.append(a)
        print(a)
    print()
    return(ltrs)

def decreate_asc(ltrs):
    a = ""
    for i in ltrs:
        x = ""
        for j in i:
            x += j
        x = int(x, 2)
        a += chr(x)
    return(a)

def create_mistakes(ltrs):
    print("Версия с ошибками:")
    ltrs2 = ltrs.copy()
    for i in  range(0, len(ltrs2)):
        j = rand(0, 7)
        ltr = ltrs2[i]
        ltr = ltr[:j] + str((int(ltr[j]) + 1) % 2) + ltr[j+1:]
        ltrs2[i] = ltr
        print(ltr)
    print()
    return(ltrs2)

def create_hams(ltrs):
    hams = []
    for i in ltrs:
        ham = []
        ham.append('x')
        ham.append('x')
        k = 3
        for j in i:
            if (log(k, 2) % 1 == 0):
                ham.append('x')
                k += 1
            ham.append(j)
            k += 1
        hams.append(ham)
    return(hams)

def decreate_hams(hams):
    ltrs = []
    for i in hams:
        a = ''
        cnt = 1
        for j in i:
            if ((log(cnt, 2) % 1) != 0):
                a += j
            cnt += 1
        if (a != ''):
            ltrs.append(a)
    return(ltrs)

def create_keys(hams):
    keys = []
    for i in hams:
        key = []
        cnt = 1
        for j in i:
            if (log(cnt, 2) % 1 == 0):
                x = 0
                k1 = cnt
                while (k1 < len(i)+1):
                    k2 = k1
                    while ((k2 < k1 + cnt) and (k2 < len(i)+1)):
                        if (i[k2-1] != 'x'):
                            x += int(i[k2-1])
                        k2 += 1
                    k1 += cnt*2
                key.append(str(x % 2))
            cnt += 1
        if (key):
            keys.append(key)
    return(keys)

def transform_hams(hams, keys):
    cnti = 0
    for i in hams:
        cntj = 0
        cntjk = 0
        for j in i:
            if (j == 'x'):
                hams[cnti][cntj] = keys[cnti][cntjk]
                cntjk += 1
            cntj += 1
        cnti += 1
    return(hams)

def fix_hams(hams):
    j = 0
    hamsN = []
    keys = create_keys(hams)
    nums = []
    for i in keys:
        num = ''
        for j in i:
            num += j
        nums.append(int(num[::-1], 2))
    cnt = 0
    for i in hams:
        i[nums[cnt] - 1] = str((int(i[nums[cnt] - 1]) + 1) % 2)
        cnt += 1
        hamsN.append(i)
    return(hamsN)
