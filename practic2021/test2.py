import docx
from docx.enum.shape import WD_INLINE_SHAPE
from io import BytesIO
from tkinter import *



def text_check(doc, shablon):
    i = -1
    for prgr in shablon.paragraphs:
        i += 1
        rns = prgr.runs
        hd_text = ""
        hd2_text = ""
        new_text = ""
        mode = 1
        if (prgr.text != ""): 
            for rn in prgr.runs:
                if (rn.font.highlight_color == None) and (mode == 1):
                    hd_text += rn.text
                elif (rn.font.highlight_color != None) and ((mode == 1) or (mode == 2)):
                    mode = 2
                elif (rn.font.highlight_color == None) and ((mode == 2) or (mode == 3)):
                    if (rn.text != "."):
                        mode = 3
                    hd2_text += rn.text
                elif (rn.font.highlight_color != None) and (mode == 3):
                    mode = 2
                    new_text = find_rn(doc, i, hd_text, hd2_text)
                    prgr.text = prgr.text[:prgr.text.find(hd_text)+len(hd_text)] + new_text + prgr.text[prgr.text.find(hd2_text):]
                    hd_text = hd2_text
                    hd2_text = ""
            if (mode == 1):
                prgr.text = hd_text
            elif (mode == 2):
                new_text = find_rn(doc, i, hd_text, hd2_text)
                prgr.text = prgr.text[:prgr.text.find(hd_text)+len(hd_text)] + new_text + hd2_text
            elif (mode == 3):
                new_text = find_rn(doc, i, hd_text, hd2_text)
                prgr.text = prgr.text[:prgr.text.find(hd_text)+len(hd_text)] + new_text + prgr.text[prgr.text.find(hd2_text):]
    return(shablon)



def find_rn(doc, i, hd_text, hd2_text):
    if (i < len(doc.paragraphs)):
        if (doc.paragraphs[i].text.find(hd_text) != -1):
            if (doc.paragraphs[i].text.find(hd2_text) != -1) and (hd2_text != "."):
                new_text = doc.paragraphs[i].text[doc.paragraphs[i].text.find(hd_text)+len(hd_text):doc.paragraphs[i].text.find(hd2_text)]
            elif (doc.paragraphs[i].text.find(hd2_text) != -1):
                new_text = doc.paragraphs[i].text[doc.paragraphs[i].text.find(hd_text)+len(hd_text):-1]
            else:
                global button_chk
                global window
                button_chk = True
                while button_chk:
                    window2 = Toplevel(window)
                    window2.geometry('700x200')
                    Label( wn, text=("ТРЕБУЕТСЯ ВВОД:   " + hd_text)).grid( row=0 )
                    message = StringVar()
                    message_entry = Entry(window2, textvariable = message)
                    message_button = Button(window2, text="Click Me", command=toogle_button)
                    message_entry.pack()
                    message_button.pack()
                new_text = message.get()
                wn.destroy()
                #new_text = input("ТРЕБУЕТСЯ ВВОД:   " + hd_text)
        else:
            new_text = find_rn2(doc, hd_text, hd2_text)
    else:
        new_text = find_rn2(doc, hd_text, hd2_text)
    return(new_text)



def toogle_button():
    global button_chk
    button_chk = False



def find_rn2(doc, hd_text, hd2_text):
    for prgr in doc.paragraphs:
        if (prgr.text.find(hd_text) != -1):
            if (prgr.text.find(hd2_text) != -1):
                new_text = prgr.text[prgr.text.find(hd_text)+len(hd_text):prgr.text.find(hd2_text)]
                return(new_text)
            else:
                new_text = input("ТРЕБУЕТСЯ ВВОД:   " + hd_text)
                return(new_text)
    new_text = input("ТРЕБУЕТСЯ ВВОД:   " + hd_text)
    return(new_text)



def tables_check(doc, shablon):
    num_table = -1
    for tbl in shablon.tables:
        num_table += 1
        hd_texts = []
        for rw in tbl.rows:
            for cl in rw.cells:
                font_chk = False
                for prgr in cl.paragraphs:
                    for rn in prgr.runs:
                        if (rn.font.highlight_color == None):
                            font_chk = True
                if font_chk:
                    hd_texts.append(cl.text)
        cnt_chk = 0
        num_doc_table = -1
        for doc_tbl in doc.tables:
            num_doc_table += 1
            cnt = 0
            for doc_cl in doc_tbl.rows[0].cells:
                for hd_text in hd_texts:
                    if (hd_text == doc_cl.text):
                        cnt += 1
                        break
            for doc_rw in doc_tbl.rows:
                for hd_text in hd_texts:
                    if (hd_text == doc_rw.cells[0].text):
                        cnt += 1
                        break
            cnt = cnt * 100 / len(hd_texts)
            if (cnt >= 70) and (cnt > cnt_chk):
                cnt_chk = cnt
                num_doc_table_chk = num_doc_table
        if (cnt_chk == 0):
            print("Новая таблица! Требуется полное заполнение!")
            for i in range(len(tbl.rows)):
                for j in range(len(tbl.rows[i].cells)):
                    font_chk = False
                    for prgr in tbl.rows[i].cells[j].paragraphs:
                        for rn in prgr.runs:
                            if (rn.font.highlight_color != None):
                                font_chk = True
                    if (font_chk):
                        if (i > 0) and (j > 0):
                            print()
                            print("Номер таблицы: ", num+1)
                            print("Текст строки: " + tbl.rows[i].cells[0].text)
                            print("Текст столбца: " + tbl.rows[0].cells[j].text)
                            tbl.rows[i].cells[j].text = input("Ячейка: ")
                        elif (i > 0) and (j == 0):
                            print()
                            print("Номер таблицы: ", num+1)
                            print("Номер строки: ", i+1)
                            print("Текст столбца: " + tbl.rows[0].cells[0].text)
                            tbl.rows[i].cells[j].text = input("Ячейка: ")
                        else:
                            print()
                            print("Номер таблицы: ", num+1)
                            print("Номер строки: ", 1)
                            print("Номер столбца: ", j+1)
                            tbl.rows[i].cells[j].text = input("Ячейка: ")
        else:
            tbl_hd_chk = False
            for rw in tbl.rows[1:]:
                for prgr in rw.cells[0].paragraphs:
                    for rn in prgr.runs:
                        if (rn.font.highlight_color == None):
                            tbl_hd_chk = True
            if tbl_hd_chk:
                shablon.tables[num_table] = fill_cls2HD(doc.tables[num_doc_table_chk], tbl, num_table)
            else:
                shablon.tables[num_table] = fill_cls1HD(doc.tables[num_doc_table_chk], tbl, num_table)
    return(shablon)



def fill_cls1HD(doc_tbl, tbl, num):
    if (len(doc_tbl.rows) == len(tbl.rows)):
        for i in range(len(doc_tbl.rows)):
            tbl.rows[i].cells[0].text = doc_tbl.rows[i].cells[0].text
        for i in range(1, len(doc_tbl.rows)):
            for j in range(1, len(doc_tbl.rows[i].cells)):
                tbl.rows[i].cells[j].text = find_cl(doc_tbl, tbl, i, j)
                if (tbl.rows[i].cells[j].text == "NT_FND"):
                    print()
                    print("ТРЕБУЕТСЯ ВВОД:")
                    print("Номер таблицы: ", num+1)
                    print("Текст строки: " + tbl.rows[i].cells[0].text)
                    print("Текст столбца: " + tbl.rows[0].cells[j].text)
                    tbl.rows[i].cells[j].text = input("Ячейка: ")
        return(tbl)
    else:
        for i in range(1, len(doc_tbl.rows)):
            print()
            print("ТРЕБУЕТСЯ ВВОД:")
            print("Номер таблицы: ", num+1)
            print("Номер строки: ", i+1)
            print("Текст столбца: " + tbl.rows[0].cells[j].text)
            tbl.rows[i].cells[0].text = input("Ячейка: ")
            for i in range(1, len(doc_tbl.rows)):
                for j in range(1, len(doc_tbl.rows[i].cells)):
                    tbl.rows[i].cells[j].text = find_cl(doc_tbl, tbl, i, j)
                    if (tbl.rows[i].cells[j].text == "NT_FND"):
                        print()
                        print("ТРЕБУЕТСЯ ВВОД:")
                        print("Номер таблицы: ", num+1)
                        print("Текст строки: " + tbl.rows[i].cells[0].text)
                        print("Текст столбца: " + tbl.rows[0].cells[j].text)
                        tbl.rows[i].cells[j].text = input("Ячейка: ")
            return(tbl)



def fill_cls2HD(doc_tbl, tbl, num):
    print(doc_tbl.rows[1].cells[1].text)
    for i in range(len(tbl.rows)):
        for j in range(len(tbl.rows[i].cells)):
            font_chk = False
            for prgr in tbl.rows[i].cells[j].paragraphs:
                for rn in prgr.runs:
                    if (rn.font.highlight_color != None):
                        font_chk = True
            if (font_chk):
                if (i > 0):
                    if (j > 0):
                        tbl.rows[i].cells[j].text = find_cl(doc_tbl, tbl, i, j)
                        if (tbl.rows[i].cells[j].text == "NT_FND"):
                            print()
                            print("ТРЕБУЕТСЯ ВВОД:")
                            print("Номер таблицы: ", num+1)
                            print("Текст строки: " + tbl.rows[i].cells[0].text)
                            print("Текст столбца: " + tbl.rows[0].cells[j].text)
                            tbl.rows[i].cells[j].text = input("Ячейка: ")
                    else:
                        print()
                        print("ТРЕБУЕТСЯ ВВОД:")
                        print("Номер таблицы: ", num+1)
                        print("Номер строки: ", i+1)
                        print("Текст столбца: " + tbl.rows[0].cells[0].text)
                        tbl.rows[i].cells[j].text = input("Ячейка: ")
                else:
                    print()
                    print("ТРЕБУЕТСЯ ВВОД:")
                    print("Номер таблицы: ", num+1)
                    print("Номер строки: ", 1)
                    print("Номер столбца: ", j+1)
                    tbl.rows[i].cells[j].text = input("Ячейка: ")
    return(tbl)



def find_cl(doc_table, tbl, i, j):
    clj = -1
    for cl in doc_table.rows[0].cells:
        clj += 1
        if (cl.text == tbl.rows[0].cells[j].text):
            cli = 0
            for doc_rw in doc_table.rows[1:]:
                cli += 1
                if (doc_rw.cells[0].text == tbl.rows[i].cells[0].text):
                    return(doc_table.rows[cli].cells[clj].text)
    return("NT_FND")



def main_algorithm(doc_name, shablon_name, new_doc_name):
    shablon = text_check(docx.Document(doc_name), docx.Document(shablon_name))
    shablon = tables_check(docx.Document(doc_name), shablon)
    shablon.save(new_doc_name)
    
