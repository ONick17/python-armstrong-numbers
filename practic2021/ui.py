from tkinter import *
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
from tkinter import scrolledtext
from tkinter import Menu
import tkinter as tk
from tkinter import ttk

import docx

#Функция выхода из программы при закрытии окна
def close(event):
    raise SystemExit

#Функция обработки события при нажатии на кнопку "Выбрать файл" для документа
def btn_slct_fl1_clck():
    global lbl_slct_fl1
    global fl1_name
    Tk().withdraw()
    fl1_name = askopenfilename(filetypes = (("Word-files","*.docx"),("all files","*.*")))
    #Если пользователь не выбрал файл, название будет измененно на "none"
    if fl1_name == "":
        fl1_name = "none"
    #Отображение пути к документу в окне программы
    lbl_slct_fl1=fl1_name.name
    #lbl_slct_fl1.configure(text=fl1_name)

#Функция обработки события при нажатии на кнопку "Выбрать файл" для шаблона
def btn_slct_fl2_clck():
    global lbl_slct_fl2
    global fl2_name
    Tk().withdraw()
    fl2_name = askopenfilename(filetypes = (("Word-files","*.docx"),("all files","*.*")))
    #Если пользователь не выбрал файл, название будет измененно на "none"
    if fl2_name == "":
        fl2_name = "none"
    #Отображение пути к шаблону в окне программы
    lbl_slct_fl2.configure(text=fl2_name)
    #путь lbl_slct_fl2.configure(text=fl2_name)
    

#Функция обработки события при нажатии на кнопку "Выполнить"
def btn_do_clck():
    global fl1_name
    global fl2_name
    global txt_area
    #Проверка документа на ".docx" и вывод комментария на экран
    if ((fl1_name[-5:-1] + fl1_name[-1]) != ".docx"):
        txt_area.delete(1.0, END)
        txt_area.insert(INSERT,"Выберите документ в формате docx!")
    #Проверка шаблона на ".docx" и вывод комментария на экран
    elif ((fl2_name[-5:-1] + fl2_name[-1]) != ".docx"):
        txt_area.delete(1.0, END)
        txt_area.insert(INSERT,"Выберите шаблон в формате docx!")
    #Копирование текста из документа в шаблон и вывод текста на экран
    else:
        txt_area.delete(1.0, END)
        fl3_name = asksaveasfilename(defaultextension=".docx")
        if (fl3_name == ""):
            txt_area.insert(INSERT, "Файл сохранения не выбран!")
        else:
            fl1 = docx.Document(fl1_name)
            fl3 = docx.Document()
            for paragra in fl1.paragraphs:
                fl3.add_paragraph(paragra.text)
                txt_area.insert(INSERT, paragra.text + '\n')
            fl3.save(fl3_name)
def about():

    newWindow = tk.Toplevel(window)
    newWindow.geometry('400x400')
    text = "Вот эта вот фигня изобретена укропами. Взята, буквально с боем. Вот вы можете преставить больное воображение человека, который вот эту вот программу создал"
    info = tk.Text(newWindow, height=400, width = 400)
    info.pack()
    info.insert(tk.END,text)
    info.configure(state='disabled')
    newWindow.title("О программе")
def help():
    newWindow = tk.Toplevel(window)
    newWindow.geometry('400x400')
    text = "Чтобы загрузить файл нажмите кнопку, потом другую кнопку и потом третью кнопку"
    info = tk.Text(newWindow, height=400, width = 400)
    info.pack()
    info.insert(tk.END,text)
    info.configure(state='disabled')
    newWindow.title("Инструкция")
#создание окна
def edit_panel():
    edit_window = tk.Toplevel(window)
    edit_window.resizable(width=False, height=False)
    edit_window.title("Окно редактирования")
    edit_window.geometry('800x400')
    panel = ttk.Notebook(edit_window)
    Titul =ttk.Frame(panel)
    Titul_lbl = Label(Titul, text="Дата").grid(row=0, column=0)
    disp_lbl = Label(Titul, text="Наименование дисциплины").grid(row=1, column=0)
    napr_lbl = Label(Titul, text="Направленность (профиль) подготовки").grid(row=2, column=0)
    protocol = Label(Titul, text="Протокол").grid(row=3, column=0)
    #эту штуку можно сделать отдельным модулем для дат, но не знаю, как оно будет в документ интегрироваться date_entry= Entry(Titul).grid(row=0,column=1, padx=50)
    other_prot = Label(Titul, text="№").grid(row=3, column=2)
    other_prot2 = Label(Titul, text="от").grid(row=3, column=4)
    
    name_disp = Entry(Titul, width=40).grid(row=1,column=1, padx=50)
    napr = Entry(Titul, width = 40).grid(row=2,column=1, padx=50)
    protocol_entry= Entry(Titul).grid(row=3,column=1, padx=50)
    other_prot_entry= Entry(Titul, width=5).grid(row=3, column=3)
    other_prot_entry2 = Entry(Titul).grid(row=3, column=5)
    
    Tasks = ttk.Frame(panel)
    goals = Label(Tasks, text="Цели:").grid(row=0, column=0)
    goals_txt = scrolledtext.ScrolledText(Tasks, width=25, height=3).grid(row=0, column=1, padx=40)
    task = Label(Tasks, text="Задачи:").grid(row=1, column=0)
    tasks_txt = scrolledtext.ScrolledText(Tasks, width = 25, height = 5).grid(row=1, column=1,padx=40, pady=10)



    
    Tables = ttk.Frame(panel)

    Mesto= ttk.Frame(panel)
    description = Label(Mesto, text="II. МЕСТО ДИЦИПЛИНЫ В СТУРКТУРЕ ОПОП ВО", font=('Times', 14, 'bold')).grid()
    dispr_name = Entry(Mesto)
    dispr_name.grid(row=1, column=0)
    btn = Button(Mesto, text="show").grid()

    
    panel.add(Titul, text="Титул")
    panel.add(Tasks, text="Цели и задачи")
    panel.add(Tables, text="Придумать чёнить с таблицами")
    panel.add(Mesto, text="Место дициплины")
    panel.pack(expand=1,fill='both')
    
    
#Создание окна и переменных для названий файлов
fl1_name = "none"
fl2_name = "none"
window = Tk()
window.title("Temple")
window.geometry("950x400")
#Элементы меню
menu = Menu(window)  
new_item = Menu(menu,tearoff=0)
file_item=Menu(menu,tearoff=0)
new_item.add_command(label='О программе',command=about)
new_item.add_command(label='Инструкция',command=help)
#####
file_item.add_command(label='Окрыть документ',command=btn_slct_fl1_clck)
file_item.add_command(label='Окрыть шаблон',command=btn_slct_fl2_clck)
file_item.add_command(label='Выполнить',command =btn_do_clck)
file_item.add_command(label='Редактировать', command = edit_panel)
file_item.add_separator()
file_item.add_command(label='Выход', command=window.destroy)

#####
menu.add_cascade(label='Файл', menu=file_item)
menu.add_cascade(label='Справка', menu=new_item)


window.config(menu=menu)




#Создание фрэймов
frm_slct1 = LabelFrame(window, text="Документ")
frm_slct2 = LabelFrame(window, text="Шаблон")
frm_do = Frame(window)
#Заполнение фрэйма для выбора документа
lbl_slct_fl1 = Label(frm_slct1, text=fl1_name)
lbl_slct_fl1.grid(ipady=10)
btn_slct_fl1 = Button(frm_slct1, text="Выбрать файл", justify=CENTER, background="#140f0b", foreground="#ffffff",width=12, height=2, command=btn_slct_fl1_clck)
btn_slct_fl1.grid(pady=10)
#Заполнение фрэйма для выбора шаблона
lbl_slct_fl2 = Label(frm_slct2, text=fl2_name)
lbl_slct_fl2.grid(ipady=10)
btn_slct_fl2 = Button(frm_slct2, text="Выбрать файл",justify=CENTER, background="#140f0b", foreground="#ffffff",width=12, height=2, command=btn_slct_fl2_clck)
btn_slct_fl2.grid(pady=10)

#Заполнение фрэйма для кнопки выполнения
btn_do = Button(frm_do, text="Выполнить", background="#140f0b", foreground="#ffffff", width=10, height=2, command=btn_do_clck)
btn_do.grid()
#Создание текстового поля для вывода на экран
txt_area = scrolledtext.ScrolledText(window, width=70, height=15)
txt_area.grid()
#Вывод фрэймов и текстового поля в интерфейс
frm_slct1.grid(column=1,row=1)
frm_slct2.grid(column=1,row=2)
frm_do.grid(column=1,row=3)
txt_area.grid(row=1, column=0, rowspan=2, sticky=W, padx=5, pady=5)
#завершение программы при закрытии окна
window.bind('<Escape>', close)
#Функция для работы окна
window.mainloop()
