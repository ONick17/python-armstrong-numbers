from tkinter import *
from tkinter.filedialog import askopenfilename
from tkinter import scrolledtext 

def btn_slct_fl_clck():
    global lbl_slct_fl
    global fl_name
    Tk().withdraw()
    fl_name = askopenfilename()
    if fl_name == "":
        fl_name = "none"
    lbl_slct_fl.configure(text=fl_name)

def btn_do_clck():
    global fl_name
    global txt_area
    roles = {}
    str1 = "ы"
    cnt = 0
    with open(fl_name, encoding = "utf-8") as fl1:
        while (str1 != ""):
            str1 = fl1.readline()
            str1_fnd = str1.find(':')
            cnt += 1
            if (str1_fnd != -1):
                key = str1[0 : str1_fnd+1]
                if (roles.get(key, "None") == "None"):
                    if (str1[-1] == '\n'):
                        value = [str(cnt) + ") " + str1[str1_fnd+2 : -1]]
                    else:  value = [str(cnt) + ") " + str1[str1_fnd+2 : ]]
                    roles[key] = value
                else:
                    if (str1[-1] == '\n'):
                        value = str(cnt) + ") " + str1[str1_fnd+2 : -1]
                    else:  value = str(cnt) + ") " + str1[str1_fnd+2 : ]
                    roles[key].append(value)
    fl1.close()
    for key in roles:
        txt_area.insert(INSERT, key + '\n')
        for i in roles[key]:
            txt_area.insert(INSERT, i + '\n')
        txt_area.insert(INSERT, '\n')

fl_name = "none"
window = Tk()
window.title("Temple")
window.geometry("400x400")

frm_slct = LabelFrame(window, text="Файл")
frm_do = Frame(window)
lbl_slct_fl = Label(frm_slct, text=fl_name)
lbl_slct_fl.pack(pady="3")
btn_slct_fl = Button(frm_slct, text="Выбрать файл", command=btn_slct_fl_clck)
btn_slct_fl.pack(pady="5")
btn_do = Button(frm_do, text="Выполнить", command=btn_do_clck)
btn_do.pack(pady="5")
txt_area = scrolledtext.ScrolledText(window)
frm_slct.pack(fill="both")
frm_do.pack(fill="both")
txt_area.pack(fill="both")
window.mainloop()
