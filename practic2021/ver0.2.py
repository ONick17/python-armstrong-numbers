from tkinter import *
from tkinter.filedialog import askopenfilename
from tkinter import scrolledtext
import docx

#Функция обработки события при нажатии на кнопку "Выбрать файл" для документа
def btn_slct_fl1_clck():
    global lbl_slct_fl1
    global fl1_name
    Tk().withdraw()
    fl1_name = askopenfilename()
    #Если пользователь не выбрал файл, название будет измененно на "none"
    if fl1_name == "":
        fl1_name = "none"
    #Отображение пути к документу в окне программы
    lbl_slct_fl1.configure(text=fl1_name)

#Функция обработки события при нажатии на кнопку "Выбрать файл" для шаблона
def btn_slct_fl2_clck():
    global lbl_slct_fl2
    global fl2_name
    Tk().withdraw()
    fl2_name = askopenfilename()
    #Если пользователь не выбрал файл, название будет измененно на "none"
    if fl2_name == "":
        fl2_name = "none"
    #Отображение пути к шаблону в окне программы
    lbl_slct_fl2.configure(text=fl2_name)

#Функция обработки события при нажатии на кнопку "Выполнить"
def btn_do_clck():
    global fl1_name
    global fl2_name
    global txt_area
    #Проверка документа на ".docx" и вывод комментария на экран
    if ((fl1_name[-5:-1] + fl1_name[-1]) != ".docx"):
        txt_area.delete(1.0, END)
        txt_area.insert(INSERT,"Выберите документ в формате docx!")
    #Проверка шаблона на ".docx" и вывод комментария на экран
    elif ((fl2_name[-5:-1] + fl2_name[-1]) != ".docx"):
        txt_area.delete(1.0, END)
        txt_area.insert(INSERT,"Выберите шаблон в формате docx!")
    #Копирование текста из документа в шаблон и вывод текста на экран
    else:
        txt_area.delete(1.0, END)
        fl1 = docx.Document(fl1_name)
        fl2 = docx.Document(fl2_name)
        for paragra in fl1.paragraphs:
            fl2.add_paragraph(paragra.text)
            txt_area.insert(INSERT, paragra.text + '\n')
        fl2.save(fl2_name)

#Создание окна и переменных для названий файлов
fl1_name = "none"
fl2_name = "none"
window = Tk()
window.title("Temple")
window.geometry("400x400")
#Создание фрэймов
frm_slct1 = LabelFrame(window, text="Документ")
frm_slct2 = LabelFrame(window, text="Шаблон")
frm_do = Frame(window)
#Заполнение фрэйма для выбора документа
lbl_slct_fl1 = Label(frm_slct1, text=fl1_name)
lbl_slct_fl1.pack(pady="3")
btn_slct_fl1 = Button(frm_slct1, text="Выбрать файл", command=btn_slct_fl1_clck)
btn_slct_fl1.pack(pady="5")
#Заполнение фрэйма для выбора шаблона
lbl_slct_fl2 = Label(frm_slct2, text=fl2_name)
lbl_slct_fl2.pack(pady="3")
btn_slct_fl2 = Button(frm_slct2, text="Выбрать файл", command=btn_slct_fl2_clck)
btn_slct_fl2.pack(pady="5")
#Заполнение фрэйма для кнопки выполнения
btn_do = Button(frm_do, text="Выполнить", command=btn_do_clck)
btn_do.pack(pady="5")
#Создание текстового поля для вывода на экран
txt_area = scrolledtext.ScrolledText(window)
#Вывод фрэймов и текстового поля в интерфейс
frm_slct1.pack(fill="both")
frm_slct2.pack(fill="both")
frm_do.pack(fill="both")
txt_area.pack(fill="both")
#Функция для работы окна
window.mainloop()
