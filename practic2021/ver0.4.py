from tkinter import *
from tkinter import scrolledtext
from tkinter import Menu
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
import tkinter as tk
import docx

#Функция выхода из программы при закрытии окна
def close():
    window.quit()
    window.destroy()
    raise SystemExit

#Функция обработки события при нажатии на кнопку "Выбрать файл" для документа
def btn_slct_fl1_clck():
    global fl1_name
    Tk().withdraw()
    fl1_name = askopenfilename(filetypes = (("Word-files","*.docx"),("all files","*.*")))
    #Если пользователь не выбрал файл, название будет измененно на "none"
    if fl1_name == "":
        fl1_name = "none"
    #Отображение пути к документу в окне программы
    lbl_slct_fl1.configure(text=fl1_name)

#Функция обработки события при нажатии на кнопку "Выбрать файл" для шаблона
def btn_slct_fl2_clck():
    global fl2_name
    Tk().withdraw()
    fl2_name = askopenfilename(filetypes = (("Word-files","*.docx"),("all files","*.*")))
    #Если пользователь не выбрал файл, название будет измененно на "none"
    if fl2_name == "":
        fl2_name = "none"
    #Отображение пути к шаблону в окне программы
    lbl_slct_fl2.configure(text=fl2_name)

#Функция обработки события при нажатии на кнопку "Выполнить"
def btn_do_clck():
    global fl1_name
    global fl2_name
    #Проверка документа на ".docx" и вывод комментария на экран
    if ((fl1_name[-5:-1] + fl1_name[-1]) != ".docx"):
        txt_area.delete(1.0, END)
        txt_area.insert(INSERT,"Выберите документ в формате docx!")
    #Проверка шаблона на ".docx" и вывод комментария на экран
    elif ((fl2_name[-5:-1] + fl2_name[-1]) != ".docx"):
        txt_area.delete(1.0, END)
        txt_area.insert(INSERT,"Выберите шаблон в формате docx!")
    #Копирование текста из документа в шаблон, вывод текста на экран и в новый файл
    else:
        txt_area.delete(1.0, END)
        fl3_name = asksaveasfilename(defaultextension=".docx")
        if (fl3_name == ""):
            txt_area.insert(INSERT, "Файл сохранения не выбран!")
        else:
            fl1 = docx.Document(fl1_name)
            fl2 = docx.Document(fl2_name)
            fl3 = docx.Document()
            while (len(fl1.paragraphs)<len(fl2.paragraphs)):
                zat = fl1.add_paragraph("ЗАТЫЧКА")
                zat.style.font.bold = True
                zat.style.font.italic = True
            while (len(fl1.paragraphs)>len(fl2.paragraphs)):
                zat = fl2.add_paragraph("ЗАТЫЧКА")
                zat.style.font.bold = True
                zat.style.font.italic = True
            for par1, par2 in zip(fl1.paragraphs, fl2.paragraphs):
                par3 = fl3.add_paragraph()
                txt_area.insert(INSERT, '\n')
                while (len(par1.runs)<len(par2.runs)):
                    zat = par1.add_run("ЗАТЫЧКА")
                    zat.font.bold=True
                    zat.font.italic=True
                while (len(par1.runs)>len(par2.runs)):
                    zat = par2.add_run("ЗАТЫЧКА")
                    zat.font.bold=True
                    zat.font.italic=True
                chk = True
                for rn1, rn2 in zip(par1.runs, par2.runs):
                    if (rn1.font.highlight_color == None):
                        if (rn1.text == rn2.text):
                            chk = True
                        else:
                            chk = False
                        par3.add_run(rn1.text)
                        txt_area.insert(INSERT, rn1.text + '\n')
                        txt_area.insert(INSERT, rn2.text + '\n' + '\n')
                    else:
                        txt_area.insert(INSERT, rn1.text + '\n')
                        if chk:
                            par3.add_run(rn1.text)
                            txt_area.insert(INSERT, rn2.text + '\n' + '\n')
                        else:
                            par3.add_run("НЕ СОВПАЛО ")
                            txt_area.insert(INSERT, "НЕ СОВПАЛО " + '\n' + '\n')
            styles = []
            for paragraph in fl2.paragraphs:
                styles.append(paragraph.style)
            for i in range(len(fl3.paragraphs)):
                fl3.paragraphs[i].style = styles[i]
            fl3.save(fl3_name)

def about():
    newWindow = tk.Toplevel(window)
    newWindow.geometry('400x400')
    text = "Вот эта вот фигня изобретена укропами. Взята, буквально с боем. Вот вы можете преставить больное воображение человека, который вот эту вот программу создал"
    info = tk.Text(newWindow, height=400, width = 400)
    info.pack()
    info.insert(tk.END,text)
    info.configure(state='disabled')
    newWindow.title("О программе")
    
def help():
    newWindow = tk.Toplevel(window)
    newWindow.geometry('400x400')
    text = "Чтобы загрузить файл нажмите кнопку, потом другую кнопку и потом третью кнопку"
    info = tk.Text(newWindow, height=400, width = 400)
    info.pack()
    info.insert(tk.END,text)
    info.configure(state='disabled')
    newWindow.title("Инструкция")

#Создание окна и переменных для названий файлов
fl1_name = "none"
fl2_name = "none"
window = Tk()
window.title("Temple")
window.geometry("950x400")
#Элементы меню
menu = Menu(window)  
new_item = Menu(menu,tearoff=0)
file_item=Menu(menu,tearoff=0)
new_item.add_command(label='О программе',command=about)
new_item.add_command(label='Инструкция',command=help)
#####
file_item.add_command(label='Окрыть документ',command=btn_slct_fl1_clck)
file_item.add_command(label='Окрыть шаблон',command=btn_slct_fl2_clck)
file_item.add_command(label='Выполнить',command =btn_do_clck)
file_item.add_separator()
file_item.add_command(label='Выход', command=window.destroy)
#####
menu.add_cascade(label='Файл', menu=file_item)
menu.add_cascade(label='Справка', menu=new_item)
window.config(menu=menu)
frm_slct1 = LabelFrame(window, text="Документ")
frm_slct2 = LabelFrame(window, text="Шаблон")
frm_do = Frame(window)
#Заполнение фрэйма для выбора документа
lbl_slct_fl1 = Label(frm_slct1, text=fl1_name)
lbl_slct_fl1.grid(ipady=10)
btn_slct_fl1 = Button(frm_slct1, text="Выбрать файл", justify=CENTER, background="#555", foreground="#ccc", command=btn_slct_fl1_clck)
btn_slct_fl1.grid(pady=10)
#Заполнение фрэйма для выбора шаблона
lbl_slct_fl2 = Label(frm_slct2, text=fl2_name)
lbl_slct_fl2.grid(ipady=10)
btn_slct_fl2 = Button(frm_slct2, text="Выбрать файл",justify=CENTER, background="#555", foreground="#ccc", command=btn_slct_fl2_clck)
btn_slct_fl2.grid(pady=10)
#Заполнение фрэйма для кнопки выполнения
btn_do = Button(frm_do, text="Выполнить", background="#555", foreground="#ccc", command=btn_do_clck)
btn_do.grid()
#Создание текстового поля для вывода на экран
txt_area = scrolledtext.ScrolledText(window, width=70, height=15)
txt_area.grid()
#Вывод фрэймов и текстового поля в интерфейс
frm_slct1.grid(column=1,row=1)
frm_slct2.grid(column=1,row=2)
frm_do.grid(column=1,row=3)
txt_area.grid(row=1, column=0, rowspan=2, sticky=W, padx=5, pady=5)
#завершение программы при закрытии окна
window.protocol("WM_DELETE_WINDOW", close)
#Функция для работы окна
window.mainloop()
